package main

import (
    "github.com/astaxie/beego"
)

type MainController struct {
    beego.Controller
}

func (this *MainController) Get(name string) {
    this.Ctx.WriteString("Hello"+name)
}

func main() {
    beego.Router("/", &MainController{})
    beego.Run()
}