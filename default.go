package controllers

import (
	"github.com/astaxie/beego"
)

type MainController struct {
	beego.Controller
}

func (c *MainController) Get() {
	c.Data["Website"] = "beego.me"
	c.Data["Email"] = "astaxie@gmail.com"
	c.TplName = "index.tpl"
}

func (c *MainController) HelloSitepoint() {
	//c.Data["Website"] = "My Website"
	//c.Data["Email"] = "your.email.address@example.com"
	//c.TplName = "index.tpl"

	c.Data["Str"] = "Hello " + c.Ctx.Input.Param(":id")
	c.TplName = "default/hello-sitepoint.tpl"
	c.RenderString()
}

func (a *MainController) GetControl() {
	a.TplName = "Client.tpl"
}
