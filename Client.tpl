<html>
<head>
  <title>Hello World</title>
</head>
<body>
 
Name: <input id="name">

<button id="say">OK!</button>
 
<hr>
<div id="result"></div>
 
<script>
function say() {
    var name = document.getElementById('name').value;
    
    var xhr = new XMLHttpRequest();

    var str = 'hello/' + name;
    xhr.open('GET', str, false);
    xhr.send();
    document.getElementById('result').innerHTML = xhr.responseText;
}
 
document.getElementById('say').addEventListener('click', say);
</script>
 
</body>
</html>