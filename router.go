package routers

import (
	"Server/controllers"

	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/", &controllers.MainController{})
	beego.Router("hello/:id([0-9a-zA-Zа-яА-Я]*)", &controllers.MainController{}, "get:HelloSitepoint")
	beego.Router("client", &controllers.MainController{}, "get:GetControl")
}
