package services

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"time"
)

const (
	DRIVER_NAME = "postgres"
	DB_USER     = "postgres"
	DB_PASSWORD = "root"
	DB_NAME     = "BeeGoProject"
	TABLE_NAME  = "Texts"
)

func Get() []Text {
	dbinfo := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable",
		DB_USER, DB_PASSWORD, DB_NAME)
	db, err := sql.Open(DRIVER_NAME, dbinfo)
	checkErr(err)

	rows, err := db.Query("SELECT * FROM \"" + TABLE_NAME + "\"")
	checkErr(err)

	var date time.Time
	var textBefore string
	var textAfter string

	var array = []Text{}

	for rows.Next() {
		err := rows.Scan(&date, &textBefore, &textAfter)
		checkErr(err)

		var text = Text{
			Date: date.Format("2006-01-02 15:04:05"),
			TextBefore: textBefore,
			TextAfter: textAfter,
		}
		array = append(array, text)
	}

	_ = rows.Close()

	return array
}

func Post(textBefore string, textAfter string) {
	dbinfo := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable",
		DB_USER, DB_PASSWORD, DB_NAME)
	db, err := sql.Open(DRIVER_NAME, dbinfo)
	checkErr(err)

	result, err := db.Exec("INSERT INTO \"" + TABLE_NAME + "\" VALUES ($1, $2, $3)", time.Now(), textBefore, textAfter)
	checkErr(err)

	if result != nil {
		fmt.Print("\nInserted\n")
	}
	return
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

type Text struct {
	Date string `json:"date"`
	TextBefore string `json:"textBefore"`
	TextAfter string `json:"textAfter"`
}