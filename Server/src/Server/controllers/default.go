package controllers

import (
	"Server/services"
	"github.com/astaxie/beego"
)

type MainController struct {
	beego.Controller
}

func (c *MainController) Get() {
	c.Ctx.ResponseWriter.Header().Add("Access-Control-Allow-Origin", "*")
	c.Ctx.ResponseWriter.Header().Add("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")

	c.Data["json"] = services.Get()
	c.TplName = "default/get.tpl"

	c.ServeJSON()
}

func (c * MainController) Post() {
	c.Ctx.ResponseWriter.Header().Add("Access-Control-Allow-Origin", "*")
	c.Ctx.ResponseWriter.Header().Add("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")

	var textBefore = c.GetString("textBefore")
	var textAfter = c.GetString("textAfter")

	services.Post(textBefore, textAfter)

	c.Ctx.Output.SetStatus(200)
}