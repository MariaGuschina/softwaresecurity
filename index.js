'use strict';

var texts;

function Beautify()
{
    var code = document.getElementById('originalCode').value;
    
    code = style_html(code, {
        'indent_size': 2,
        'indent_char': ' ',
        'max_char': 78,
        'brace_style': 'expand',
        'unformatted': ['a', 'sub', 'sup', 'b', 'i', 'u']
      });

    document.getElementById('beautifiedCode').value = code;
}

function GetTexts()
{
    var response = Get();
    texts = JSON.parse(response);

    ClearTable();

    var table = document.getElementById("table");
    
    for(var index = 0; index < texts.length; index++)
    {
        var text = texts[index];
        var rowsCount = table.rows.length;
        var row = table.insertRow(rowsCount);

        var cell1 = row.insertCell(0);
        cell1.innerText = text.date;

        var cell2 = row.insertCell(1);
        var element = document.createElement("input");
        element.type = "button";
        element.value = "=>";
        element.setAttribute("onclick", "DisplayTexts(" + index + ");")
        cell2.append(element)
    }
}

function DisplayTexts(index)
{
    var text1 = texts[index].textBefore;
    var text2 = texts[index].textAfter;

    var textArea1 = document.getElementById("originalCode");
    var textArea2 = document.getElementById("beautifiedCode");

    textArea1.value = text1;
    textArea2.value = text2;
}

function SaveTexts()
{
    var textArea1 = document.getElementById("originalCode");
    var textArea2 = document.getElementById("beautifiedCode");

    var text1 = textArea1.value;
    var text2 = textArea2.value;

    Post(text1, text2);
}

function ClearTable()
{
    var table = document.getElementById("table");

    for(var index = 0; index < table.rows.length; index++)
    {
        var row = table.rows[index--];
        row.parentNode.removeChild(row);
    }
}

function Get()
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", "http://localhost:8080/", false);
    xmlHttp.send();
    return xmlHttp.responseText;
}

function Post(textAfter, textBefore)
{
    var params = new FormData();
    params.append("textBefore", textBefore);
    params.append("textAfter", textAfter);

    fetch("http://localhost:8080/", {
        method: "POST",
        body: params
    }).then(GetTexts);
}